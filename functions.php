<?php
/**
 * Astra Child Theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child Theme
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_THEME_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_THEME_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

//-------------------------------
// Custom script in footer 
//-------------------------------

function custom_script_name(){
?>
<script>

jQuery( document ).ready(function() {
	// Smooth scroll to element. Not necessary but provides a bit of delight
	jQuery(".scrolltop").click(function() {
		// This prevents the default 'jump to' behaviour if JavaScript is enabled,
		// whilst keeping the functionality there for when JavaScript isn't enabled
		event.preventDefault();
	    jQuery("html, body").animate({ scrollTop: jQuery(jQuery(this).attr('href')).offset().top }, 900);
	    return true;
	});
	
	jQuery('.custom-select select').on('click', function(){
		jQuery('.custom-select select.arrow').removeClass('arrow');
		jQuery(this).addClass('arrow');
	});
	jQuery('.custom-select select option').on('click', function(){
		jQuery('.custom-select select.arrow').removeClass('arrow');
	});
});

</script>
<?php
}
add_action('wp_footer', 'custom_script_name');

add_shortcode( 'view_time_graph', 'view_time_graph_function' );
function view_time_graph_function() { 
  	global $content;
  	ob_start();
  	get_template_part( 'time-graph/time-graph');
 	$output = ob_get_clean();
    return $output;
 }