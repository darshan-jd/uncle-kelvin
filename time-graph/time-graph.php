<link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Lato:ital,wght@0,300;0,400;0,700;1,400;1,700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/time-graph/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/time-graph/slick/slick-theme.css"/>
<style>
  .graph-slide{
    position: relative;
  }
  .graph-legend-col{
    position: absolute;
    top: 0;
    left: 0;
  }
  @media (max-width:1366px){
	  #legend-container-first ul li, 
	  #legend-container-Two ul li{
		border-radius: 6px !important;
		height: 32px !important;
		width: 170px !important;
		padding: 8px !important;
		margin-bottom:6px !important;
	  }
  }
  #legend-container-first ul li:nth-child(3), 
  #legend-container-first ul li:nth-child(4){
    display: none !important;
  }
  #legend-container-first ul li:nth-child(1){
    order:1;
    background-color: #ebe2d9 !important;
    color: #9f6f47 !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-first ul li:nth-child(1):hover{
    background-color: #9f6f47 !important;
    color: #ffffff !important;
  }
  #legend-container-first ul li:nth-child(1) p{
    color: #9f6f47 !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-first ul li:nth-child(1):hover p{
    color: #ffffff !important;
  }
  #legend-container-first ul li:nth-child(2){
    order:2;
    background-color: #d3dbdd !important;
    color: #294A4A !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-first ul li:nth-child(2):hover{
    order:2;
    background-color: #294A4A !important;
    color: #ffffff !important;
  }
  #legend-container-first ul li:nth-child(2) p{
    color: #294A4A !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-first ul li:nth-child(2):hover p{
    color: #ffffff !important;
  }

  #legend-container-first p, #legend-container-Two p{
    font-family: 'Lato', sans-serif !important;
    font-size: 16px;
    line-height:1;
    font-weight: normal;

  }
  @media (max-width:1366px){
	  #legend-container-first p, #legend-container-Two p{
		font-size: 14px;
	  }
  }
  #legend-container-Two ul li:nth-child(1){
    background: transparent !important;
    color: rgb(159, 111, 71) !important;
    border-bottom: 3px solid rgb(159, 111, 71) !important;
    border-radius: 0 !important;
    padding: 0 !important;
    order: 3;
  }
  #legend-container-Two ul li:nth-child(1) > p{
    color: rgb(159, 111, 71) !important;
  } 
  #legend-container-Two ul li:nth-child(2){
    order:2;
    background-color: #d3dbdd !important;
    color: #294A4A !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-Two ul li:nth-child(2):hover{
    background-color: #294A4A !important;
    color: #ffffff !important;
  }
  #legend-container-Two ul li:nth-child(2) p{
    color: #294A4A !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-Two ul li:nth-child(2):hover p{
    color: #ffffff !important;
  }
  #legend-container-Two ul li:nth-child(3){
    order:1;
    background-color: #ebe2d9 !important;
    color: #9f6f47 !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-Two ul li:nth-child(3):hover{
    background-color: #9f6f47 !important;
    color: #ffffff !important;
  }
  #legend-container-Two ul li:nth-child(3) p{
    color: #9f6f47 !important;
    transition: all 0.3s ease-out;
  }
  #legend-container-Two ul li:nth-child(3):hover p{
    color: #ffffff !important;
  }
  #legend-container-Two ul li:nth-child(4){
    display: none !important;
  }
  @media (max-width:1300px){
    .graph-legend-col{
      position: relative;
      margin-bottom: 30px;
    }
    .graph-legend-col ul{
      justify-content:center;
      align-items: center;
      flex-direction: row !important;
    }
    .graph-legend-col ul li{
      margin: 0 8px !important;
    }
  }
  .slick-prev {
        left: 0;
  }
  .slick-next {
    right: 0;
  }
  .graph-title {
    max-width: 760px;
    padding: 0 50px;
    margin: 0 auto 40px;
  }
  @media (max-width:1366px){
	.graph-title {
    margin: 0 auto 30px;
  }
  }
  
  .graph-title h2{
    margin: 0 0 15px;
    font-family: 'Cormorant Garamond', serif;
    font-size: 35px;
    line-height: 33px;
    color: #294a4a;
      font-weight: 500;
  }
  @media (max-width:1366px){
	  .graph-title h2 {
		margin: 0 0 10px;
		font-size: 30px;
		line-height: 32px;
	}
  }
  .graph-title p{
      color: #294A4A;
      font-weight: 500;
      font-size:15px;
      line-height:1.8;
      margin: 0;
      font-family: 'Poppins', sans-serif;
  }
  @media (max-width:1366px){
	 .graph-title p{
      font-size:14px;
	}
  }
  .graph-title .delay-subtitle-text{
    font-style: italic;
    font-weight: 400;
    opacity: 0;
  }
  .graph-slide.slick-active .graph-title p.delay-subtitle-text{
    animation: fadeIn 3s ease-in-out 0s forwards;
  }
  .graph-bottom-text p{
      color: #294A4A;
      font-weight: 400;
      font-size:18px;
      line-height:22px;
      margin: 0;
      font-family: 'Lato', sans-serif;
  }
  .slider-arrows {
      position: absolute;
      max-width: 830px;
      display: flex;
      justify-content: space-between;
      width: 100%;
      margin: 0 auto;
      left: 50%;
      top: 12px;
      transform: translateX(-50%);
      z-index: 1;
      padding: 0 25px;
  }
  .slider-arrows .slick-arrow{
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    cursor: pointer;
  }
  
  .slider-arrows .slick-nav{
    display: inline-block;
    width: 1.4rem;
    height: 1.4rem;
    border: 0.225rem solid;
    border-bottom: 0;
    border-left: 0;
    border-radius: 1px;
    border-color: #ededed;
  }
  .slider-arrows .slick-nav.prev-arrow{
    transform: rotate(225deg);
  }
  .slider-arrows .slick-nav.next-arrow{
    transform: rotate(45deg);
  }
/* .chart-container{
  height: 90vh;
} */
/* .slick-slider, .slick-list, .graph-slide .row {
    height: 100%;
} */
.graph-bottom-content{
  margin-top: 35px;
  min-height: 88px;
}
@media (max-width:1366px){
	.graph-bottom-content{
	   margin-top: 20px;
	  min-height: 80px;
	}
 }
.bottom-text{
  display: none;
  text-align: center;
  color: #294A4A;
  font-weight: 400;
  font-size:18px;
  line-height:22px;
  margin: 0;
  font-family: 'Lato', sans-serif;
  max-width: 1000px;
  margin-left: auto;
  margin-right: auto;
}
@media (max-width:1366px){
	.bottom-text{
	  font-size:16px;
	  line-height:20px;
	}
 }
.bottom-text p{
  text-align: center;
  color: #294A4A;
  font-weight: 400;
  font-size:18px;
  line-height:22px;
  margin: 0;
  font-family: 'Lato', sans-serif;
}
@media (max-width:1366px){
	.bottom-text p{
	  font-size:16px;
	  line-height:20px;
	}
}
.bottom-text-show{
  display: block !important;
  animation: fadeIn 2s;
}
.graph-col{
  opacity:0;
  width: 100%;
  margin: 0 auto;
  padding: 0 63px;
}
.graph-slide.slick-active .graph-col{
  animation: fadeInUp 2s ease-in-out 0s forwards;
}
.graph-legend-col{
  opacity:0;
}
.graph-slide.slick-active .graph-legend-col{
  animation: fadeIn 2s ease-in-out 0s forwards;
}
.graph-bottom-content{
  margin-top: 25px;
}
.graph-two-block {
    position: relative;
}
.graph-two-block .age-label{
  position: absolute;
  display: inline-block;
  font-size: 15px;
  line-height: 1;
  font-weight: 600;
  font-family: 'Lato', sans-serif;
  color: #294A4A;
  z-index: 10;
  bottom: 25px;
  left: -35px;
}
.graph-two-block .ben-age-label{
  position: absolute;
  display: inline-block;
  font-size: 15px;
  line-height: 1;
  font-weight: 600;
  font-family: 'Lato', sans-serif;
  color: #294A4A;
  z-index: 10;
  bottom: 25px;
  left: -60px;
}
@keyframes fadeIn {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
@keyframes fadeInUp {
  from { 
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: none;
    transform: none;
  }
}

.canvas-after{
	position: relative;
	
}
.canvas-after:after{
	content: '';
	position: absolute;
	right: -1px;
	width: 11vw;
	height: 100%;
	bottom: 33px;
	background: -moz-linear-gradient(left,  rgba(255,255,255,0) 0%, rgba(255,255,255,0.07) 7%, rgba(255,255,255,0.45) 45%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(left,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.07) 7%,rgba(255,255,255,0.45) 45%,rgba(255,255,255,1) 100%);
	background: linear-gradient(to right,  rgba(255,255,255,0) 0%,rgba(255,255,255,0.07) 7%,rgba(255,255,255,0.45) 45%,rgba(255,255,255,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=1 );

}
.graph-cta-btn{
  text-align: center;
  margin-top:25px;
}
.graph-cta-btn .cta-btn {
    background-color: #294A4A;
    padding: 19px 20px;
    border-radius: 15px;
    font-size: 13px;
    line-height: 1;
    color: #ffffff;
    text-decoration: none;
    font-weight: 400;
    display: inline-block;
    transition: all .3s;
    font-family: 'Lato', sans-serif;
    max-width:240px;
    width: 100%;
    margin: 0 auto;
}
.graph-cta-btn .cta-btn:hover, .graph-cta-btn .cta-btn:focus {
    background-color: #9E6F47;
}

/* Graph Popup Design  Start  */
.pum-theme-3910 {
    background-color: rgba(0, 0, 0, 0.7) !important;
}
.pum-theme-3909 .pum-container, .pum-theme-default-theme .pum-container {
    border-radius: 12px !important;
    background-color: #ffffff !important;
}
.pum-theme-3910 .pum-container, .pum-theme-lightbox .pum-container, .pum-theme-3910 #popmake-3920{
	border-radius: 15px !important;
    background-color: #ffffff !important;
	border: 1px solid #707070 !important;
  margin-top:2vh !important;
}
.pum-theme-3909 .pum-content + .pum-close, .pum-theme-default-theme .pum-content + .pum-close {
    right: 15px !important;
    top: 15px !important;
}

.pum-theme-3910 .pum-content + .pum-close, .pum-theme-lightbox .pum-content + .pum-close {
    background-color: transparent !important;
    border: 0 !important;
    width: 32px !important;
    height: 32px !important;
    right: 25px !important;
    top: 25px !important;
    box-shadow: none !important;
    border-radius: 0 !important;
}


.pum-theme-3910 .pum-content + .pum-close:before, .pum-theme-3910 .pum-content + .pum-close:after {
    content: "";
    position: absolute;
    display: block;
    transition: all 0.1s;
}


.pum-theme-3910 .pum-content + .pum-close:before{
    border: 1px solid #E6E6E6;
    width: 0;
    border-width: 1px;
    border-radius: 0;
    transform: rotate(45deg);
    top: 0;
    left: 15px;
    height: 32px;
}
.pum-theme-3910 .pum-content + .pum-close:after {
    height: 2px;
    background: #E6E6E6;
    left: 15px;
    transform: rotate(45deg);
    width: 32px;
    left: 0px;
    top: 15px;
}
.pum-theme-3910 .pum-content + .pum-close:focus{
  outline:0 !important;
  box-shadow:none !important;
}
.pum-content.popmake-content:focus{
	outline:0 !important;
}
/* Graph Popup Design  End  */
.graph-slide.slick-slide:focus {
    outline: none !important;
}
</style>

<div class="container-fluid chart-container">
  <div class="slider-arrows">
        <span class="prev-arrow slick-nav"></span>
        <span class="next-arrow slick-nav"></span>
  </div>
    <div class="graph-slider">
      <div class="graph-slide">
        <div class="graph-title text-center">
          <h2>Financial planning for your future generations</h2>  
          <p class="text-uppercase">AN INTERACTIVE EXAMPLE PROJECTION</p>
          <p class="delay-subtitle-text">mouse over graph for more information</p>
        </div>
        <div class="graph-row">
          <div class="graph-legend-col">
            <div id="legend-container-first"></div> 
          </div> 
          <div class="graph-col">
            <div class="graph-two-block">
			        <div class="canvas-after">
                <canvas id="graphOne" height="85vh"></canvas>
			        </div>
              <div class="ben-age-label">Ben Age:</div>
            </div>
            
            
            <!-- Chart One Bottom Content Start -->
            <div class="graph-bottom-content">
              <div id="data1" class="bottom-text">
                <p>At age 35, Ben and his wife gave birth to a baby boy. He wanted to give his newborn son 
                  the best education possible, so he found a flexible savings plan with the help of Uncle Kelvin. 
                  For 5 years he paid an annual premium of <strong>US$25,000 per year.</strong></p>
              </div>
              <div id="data2" class="bottom-text">
                <p>After 5 years, Ben has finished making all the payments and the policy is worth <strong>US$125,000.</strong></p>
              </div>
              <div id="data3" class="bottom-text">
                <p>13 years after finishing paying all the premiums, the policy projected value is at <strong>US$233,980.</strong><br>
                  By this time, Ben’s son is ready to attend university. To fund his education, Ben decides to withdraw <strong>US$37,500</strong> from the policy 
                  annually over the course of 4 years, totalling <strong>US$150,000</strong></p>
              </div>
              <div id="data4" class="bottom-text">
                <p>Ben sees his son graduate, comfortable in the knowledge that the policy value is still <strong>US$111,412</strong> and will continue to accumulate over the years to come.</p>
              </div>
              <div id="data5" class="bottom-text">
                <p>His son has a good career and Ben does not need to withdraw cash from the policy to provide him with some financial support. <br>
                  Instead, he chooses to let the policy value continue to accumulate.</p>
              </div>
              <div id="data6" class="bottom-text">
                <p>Ben can withdraw <strong>US$684,502</strong> in full or in part for his retirement if he wants, <br>
                  or choose to change the lige insured to his son and let the policy value continue to accumulate.</p>
              </div>
            </div>
            <!-- Chart One Bottom Content End -->
            <div class="graph-cta-btn">
              <a href="#" class="cta-btn graph-one-cta pum-close popmake-close">Contact Kelvin about this plan</a>
            </div>
          </div> 
        </div>
      </div>
      <div class="graph-slide">
        <div class="graph-title text-center">
          <h2>Critical illness protection</h2> 
          <p class="text-uppercase">AN INTERACTIVE WORST-CASE SCENARIO EXAMPLE PROJECTION</p>
          <p class="delay-subtitle-text">mouse over graph for more information</p>
        </div>
        <div class="graph-row">
          <div class="graph-legend-col">
            <div id="legend-container-Two"></div> 
          </div> 
          <div class="graph-col">
            <div class="graph-two-block">
              <canvas id="canvas" height="85vh"></canvas>
              <div class="age-label">Age</div>
            </div>
            <div class="graph-bottom-content">
                  <!-- Chart Two Bottom Content Start -->
                  <div id="graph_data_1" class="bottom-text">
                    <p>Mr Lam bought critical illness protection for himself with a notional amount of HK$1,000,000 when he was 30.<br>
                      He chose to pay the premium for 25 years, but the coverage starts as soon as he completes the first annual payment.</p>
                  </div>
                  <div id="graph_data_2" class="bottom-text">
                    <p>Diagnosed with new infectious disease and stay in an ICU of a hospital for 3 days.<br>
                      ICU benefit = <strong>HK$200,000</strong> (20% of original amount paid)</p>
                  </div>
                  <div id="graph_data_3" class="bottom-text">
                    <p>Diagnosed with lung cancer within the first 10 policy years.<br>
                      Major CI benefit +additional major CI benefit = HK$1,300,000 (130% of original amount paid)</p>
                  </div>
                  <div id="graph_data_4" class="bottom-text">
                    <p>Still suffering from cancer and receiving active treatment.<br>
                      Cancer treatment booster = <strong>HK$300,000</strong> (30% of original amount paid)</p>
                  </div>
                  <div id="graph_data_5" class="bottom-text">
                    <p>Still suffering from cancer and receiving active treatment.<br>
                      Cancer treatment booster = <strong>HK$300,000</strong> (30% of original amount paid)</p>
                  </div>
                  <div id="graph_data_6" class="bottom-text">
                    <p>Lung Cancer persisted<br>
                      Cancer continuous care benefit = <strong>HK$1,000,000</strong> (100% of original amount paid)</p>
                  </div>
                  <div id="graph_data_7" class="bottom-text">
                    <p>Still suffering from cancer and receiving active treatment.<br>
                      Cancer treatment booster = <strong>HK$300,000</strong> (30% of original amount paid)</p>
                  </div>
                  <div id="graph_data_8" class="bottom-text">
                    <p>Still suffering from cancer and receiving active treatment.<br>
                      Cancer treatment booster = <strong>HK$300,000</strong> (30% of original amount paid</p>
                  </div>
                  <div id="graph_data_9" class="bottom-text">
                    <p>Diagnosed with metastatic liver cancer<br>
                      Cancer continuous care benefit = <strong>HK$1,000,000</strong> (100% of original amount paid)</p>
                  </div>
                  <div id="graph_data_10" class="bottom-text">
                    <p>Still suffering from cancer and receiving active treatment.<br>
                      Cancer treatment booster = <strong>HK$300,000</strong> (30% of original amount paid)</p>
                  </div>
                  <div id="graph_data_11" class="bottom-text">
                    <p>Still suffering from cancer and receiving active treatment.<br>
                      Cancer treatment booster = <strong>HK$300,000</strong> (30% of original amount paid)</p>
                  </div>
                  <div id="graph_data_12" class="bottom-text">
                    <p>Diagnosed with a stroke.<br>
                      Heart attack/stock continuous care benefit = <strong>HK$1,000,000</strong> (100% of original amount paid)</p>
                  </div>
                  <div id="graph_data_13" class="bottom-text">
                    <p>Even after making <strong>HK$6,300,000</strong> worth of claims (630% of the original amount paid), <br>
                      Mr Lam <i>is still</i> covered against one additional occurrence of heart attack or stroke and disability care booster.
                      <br><br>
                      We assume that Mr Lam fulfils the definitions and claims requirements of the benefits.</p>
                  </div>
                  <!-- Chart Two Bottom Content End -->
              </div>
              <div class="graph-cta-btn">
                <a href="#" class="cta-btn graph-two-cta pum-close popmake-close">Contact Kelvin about this plan</a>
              </div>
          </div> 
        </div>
      </div>
      
  </div>

  </div>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/time-graph/slick/slick.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-plugin-datalabels/2.0.0-rc.1/chartjs-plugin-datalabels.min.js" integrity="sha512-+UYTD5L/bU1sgAfWA0ELK5RlQ811q8wZIocqI7+K0Lhh8yVdIoAMEs96wJAIbgFvzynPm36ZCXtkydxu1cs27w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <!-- Chart One JS Start -->
  <script>
      let width, height, gradient;
      function getGradient(ctx, chartArea) {
        const chartWidth = chartArea.right - chartArea.left;
        const chartHeight = chartArea.bottom - chartArea.top;
        if (gradient === null || width !== chartWidth || height !== chartHeight) {
          // Create the gradient because this is either the first render
          // or the size of the chart has changed
          width = chartWidth;
          height = chartHeight;
          gradient = ctx.createLinearGradient(chartArea.left, 0, chartArea.right, 0);
          gradient.addColorStop(0, 'red');
          gradient.addColorStop(1,'green');
        }

        return gradient;
      };
    const getOrCreateLegendList = (chart, id) => {
      const legendContainer = document.getElementById(id);
      let listContainer = legendContainer.querySelector('ul');

      if (!listContainer) {
        listContainer = document.createElement('ul');
        listContainer.style.display = 'flex';
        listContainer.style.flexDirection = 'column';
        listContainer.style.margin = 0;
        listContainer.style.padding = 0;

        legendContainer.appendChild(listContainer);
      }

      return listContainer;
    };

    const htmlLegendPlugin = {
      id: 'htmlLegend',
      afterUpdate(chart, args, options) {
        const ul = getOrCreateLegendList(chart, options.containerID);

        // Remove old legend items
        while (ul.firstChild) {
          ul.firstChild.remove();
        }

        // Reuse the built-in legendItems generator
        const items = chart.options.plugins.legend.labels.generateLabels(chart);

        items.forEach(item => {
          const li = document.createElement('li');
        // li.style.alignItems = 'center';
          li.style.cursor = 'pointer';
          li.style.display = 'flex';
          li.style.alignItems = 'center';
          li.style.justifyContent = 'center';
          li.style.marginLeft = '0';
          li.style.background = item.fillStyle;
          li.style.borderColor = item.strokeStyle;
          li.style.color = item.fontColor;
          li.style.marginBottom = '10px';
          li.style.borderRadius = '10px';
          li.style.height = '40px';
          li.style.width = '170px';
          li.style.padding = '10px';
          
        //  li.style.padding = 0;

          li.onclick = () => {
            const {type} = chart.config;
            if (type === 'pie' || type === 'doughnut') {
              // Pie and doughnut charts only have a single dataset and visibility is per item
              chart.toggleDataVisibility(item.index);
            } else {
              chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
            }
            chart.update();
          };

          // Color box
          const boxSpan = document.createElement('span');
          boxSpan.style.background = item.fillStyle;
          boxSpan.style.borderColor = item.strokeStyle;
          boxSpan.style.borderWidth = item.lineWidth + 'px';
          boxSpan.style.display = 'inline-block';
          boxSpan.style.height = '20px';
          boxSpan.style.marginRight = '10px';
          boxSpan.style.width = '20px';

          // Text
          const textContainer = document.createElement('p');
          textContainer.style.color = item.fontColor;
          textContainer.style.margin = 0;
          textContainer.style.padding = 0;
        // textContainer.style.textDecoration = item.hidden ? 'line-through' : '';

          const text = document.createTextNode(item.text);
          textContainer.appendChild(text);

        //  li.appendChild(boxSpan);
          li.appendChild(textContainer);
          ul.appendChild(li);
        });
      }
    };
    
    var data = {
     // type: 'line',
      labels: [
        "30",
        "40",
        "53",
        "56",
        "70",
        "90",
        ""
      ],
      datasets: [
        // {
        //   label: 'Ben Age',
        //   backgroundColor: "#134B44",
        //   borderColor: "#134B44",
        //   color: "#9F6F47",
        //   type: 'line',
        //   data: [35, 40, 53, 56, 70, 90],
        //   labels: ["Ben Age: 35\n"],
        //   yAxisID: "lineax",
        //   pointRadius: 4,
        //   datalabels:{
        //     color: '#134B44',
        //     offset: '5',
        //     textAlign: 'start',
        //     align: 'top',
        //     anchor: 'end',
        //     // align: function(context) {
        //     //   if (context.dataIndex === context.dataset.data.length - 6)
        //     //   {
        //     //     return context.active ? 'left' : 'left';
        //     //   } else{
        //     //     return context.active ? 'top' : 'top';
        //     //   }
        //     // },
        //     // anchor: function(context) {
        //     //   if (context.dataIndex === context.dataset.data.length - 6)
        //     //   {
        //     //     return context.active ? 'end' : 'end';
        //     //   } else{
        //     //     return context.active ? 'end' : 'end';
        //     //   }
        //     // },
        //     // offset: function(contextt) {
        //     //   if (contextt.dataIndex === contextt.dataset.data.length - 6)
        //     //   {
        //     //     return contextt.active ? '-8' : '-8';
        //     //   }
        //     // },
        //     // formatter: function(value, context) {
        //     //   if (context.dataIndex === context.dataset.data.length - 6)
        //     //   {
                
        //     //       return context.dataset.labels[context.dataIndex];
        //     //   }
        //     // },
            
        //   }
        // },
        {
          label: "Amount withdrawn", 
          backgroundColor: "#C4C3BE",
          borderColor: "#C4C3BE",
          hoverBackgroundColor: "#294A4A",
          hoverBorderColor: "#294A4A",
          borderWidth: 1,
          categoryPercentage: 1.1,
          type: 'bar',
          borderRadius: {
            topLeft: 10,
            topRight: 10,
            bottomRight: 0,
            bottomLeft: 0,
          },
          borderSkipped: false,
          yAxisID: "bary",
          data: [0, 0, 150000, 0, 0, 0, 0],
          labels: ["", "", "-US$ 150,000", "", "", "", ""],
          datalabels:{
            color: '#ffffff',
            anchor: 'end',
            align: 'start',
            offset: '10',
           
            opacity: function(context) {
              if(context.chart.data.datasets[context.datasetIndex].data[context.dataIndex] == 0){
                return context.active ? 0 : 0; 
              } 
              return context.active ? 1 : 0; 
            },
            // formatter: (value) => {
            //   return 'US$' + ' ' + value;
            // },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
          }
        },
        {
          label: "Projected Value",
        backgroundColor: '#EBE2D9',
        //   backgroundColor: function(context) {
        //   const chart = context.chart;
        //   const {ctx, chartArea} = chart;

        //   if (!chartArea) {
        //     // This case happens on initial chart load
        //     return null;
        //   }
        //   return getGradient(ctx, chartArea);
        // },
          borderColor: [
            '#EBE2D9',
            '#EBE2D9',
            '#EBE2D9',
            '#EBE2D9',
            '#EBE2D9',
            '#c2c5bf',
            '#EBE2D9',
          ],

          hoverBackgroundColor: [
            "#9F6F47",
            "#9F6F47",
            "#9F6F47",
            "#9F6F47",
            "#9F6F47",
            "#9F6F47",
            "#EBE2D9"
        ],
          hoverBorderColor: [
            '#9F6F47',
            '#9F6F47',
            '#9F6F47',
            '#9F6F47',
            '#9F6F47',
            '#294A4A',
            '#EBE2D9'
          ],
          borderWidth: [1,1,1,1,1,10],
          borderRadius: {
            topLeft: 10,
            topRight: 10,
            bottomRight: 0,
            bottomLeft: 0,
          },
         borderSkipped: false,
          categoryPercentage: 1.1,
          type: 'bar',
          
        //  stack: 'combined',
          yAxisID: "bary",
          data: [0,125000,233980,111412,236816,684502, 1000000],
          labels: ["US$ 0", "US$ 125,000", "US$ 233,980", "US$ 111,412", "US$ 236,816", "US$ 684,502", ""],
          datalabels:{
            color: ['#9F6F47', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff'],
            anchor: 'end',
            align: 'start',
            offset: [
              '-40',
              '10',
              '6',
              '10',
              '10',
              '18',
              '10',
            ],
            opacity: function(context) {
              // if(context.chart.data.datasets[context.datasetIndex].data[context.dataIndex] == 0){
              //   return 0;
              // } 
              if (context.dataIndex === context.dataset.data.length - 1)
              {
                return context.active ? 0 : 0; 
              }
              return context.active ? 1 : 0; 
            },
            // formatter: (value) => {
            //   //return value + '%';
            //   return 'US$' + ' ' + value;
            // },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
          }
        }
        ,
        {
          label: "X data",
          labels: ["", "x 0", "x 1.9", "x 0.9", "x 1.9", "x 5.5", ""],
          type: 'bar',
          data: [1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 0],
          backgroundColor: [
              'rgba(255, 255, 255, 0)',
          ],
          borderColor: [
              'rgba(255, 255, 255, 0)',
          ],
          borderRadius: {
            topLeft: 0,
            topRight: 0,
            bottomRight: 0,
            bottomLeft: 0,
          },
         borderSkipped: false,
         categoryPercentage: 1.1,
         hoverBackgroundColor: 'rgba(245, 245, 245, 1)',
         hoverBorderColor: 'rgba(245, 245, 245, 1)',
         borderWidth: 1,
        // stack: 'combined',
        yAxisID: "bary",
          datalabels:{
            color: '#9F6F47',
            anchor: 'end',
            align: 'start',
            offset: '10',
            textAlign: 'center',
            font: {
              family: "'Lato', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
              size: 19,
              weight: 'bold',
            },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
            opacity: function(context) {
              return context.active ? 1 : 0;
            }
          }
          
        },
        {
          label: "Xtwo data",
          labels: ["", "", "-(x 1.2)", "", "", "", ""],
          type: 'bar',
          data: [0, 0, 1000000, 0, 0, 0, 0],
          backgroundColor: [
              'rgba(255, 255, 255, 0)',
          ],
          borderColor: [
              'rgba(255, 255, 255, 0)',
          ],
          borderRadius: {
            topLeft: 0,
            topRight: 0,
            bottomRight: 0,
            bottomLeft: 0,
          },
         borderSkipped: false,
         categoryPercentage: 1.1,
         hoverBackgroundColor: 'rgba(245, 245, 245, 0)',
         hoverBorderColor: 'rgba(245, 245, 245, 0)',
         borderWidth: 1,
        // stack: 'combined',
        yAxisID: "bary",
          datalabels:{
            color: '#294A4A',
            anchor: 'end',
            align: 'start',
            offset: '30',
            textAlign: 'center',
            font: {
              family: "'Lato', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
              size: 19,
              weight: 'bold',
            },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
            opacity: function(context) {
              return context.active ? 1 : 0;
            }
          }
          
        }
      ]
    };
    var delayed;
    var chartOptionsOne = {
      // responsive: true,
      // maintainAspectRatio: false,
      onHover : function(evt) {
          var activePoint = myLineChart.getElementsAtEventForMode(evt, 'nearest', {
            intersect: true
          }, false);
            document.getElementById("graph_data_1").classList.remove("bottom-text-show");
            document.getElementById("graph_data_2").classList.remove("bottom-text-show");
            document.getElementById("graph_data_3").classList.remove("bottom-text-show");
            document.getElementById("graph_data_4").classList.remove("bottom-text-show");
            document.getElementById("graph_data_5").classList.remove("bottom-text-show");
            document.getElementById("graph_data_6").classList.remove("bottom-text-show");
            document.getElementById("graph_data_7").classList.remove("bottom-text-show");
            document.getElementById("graph_data_8").classList.remove("bottom-text-show");
            document.getElementById("graph_data_9").classList.remove("bottom-text-show");
            document.getElementById("graph_data_10").classList.remove("bottom-text-show");
            document.getElementById("graph_data_11").classList.remove("bottom-text-show");
            document.getElementById("graph_data_12").classList.remove("bottom-text-show"); 
            document.getElementById("graph_data_13").classList.remove("bottom-text-show"); 

            document.getElementById("data1").classList.remove("bottom-text-show");
            document.getElementById("data2").classList.remove("bottom-text-show");
            document.getElementById("data3").classList.remove("bottom-text-show");
            document.getElementById("data4").classList.remove("bottom-text-show");
            document.getElementById("data5").classList.remove("bottom-text-show");
            document.getElementById("data6").classList.remove("bottom-text-show");
           // document.getElementById("data7").classList.remove("bottom-text-show");
          if (activePoint.length > 0) {
            var clickedElementindex = activePoint[0].index;
            if(clickedElementindex == 0){
              document.getElementById("data1").classList.add("bottom-text-show");
            } else if (clickedElementindex == 1){
             document.getElementById("data2").classList.add("bottom-text-show");
            } else if (clickedElementindex == 2){
             document.getElementById("data3").classList.add("bottom-text-show");
            } else if (clickedElementindex == 3){
             document.getElementById("data4").classList.add("bottom-text-show");
            } else if (clickedElementindex == 4){
             document.getElementById("data5").classList.add("bottom-text-show");
            } else if (clickedElementindex == 5){
             document.getElementById("data6").classList.add("bottom-text-show");
            } 
          }
        },
       
      animation: {
        onComplete: () => {
          delayed = true;
        },
        delay: (context) => {
          let delay = 0;
          if (context.type === 'data' && context.mode === 'default' && !delayed) {
            delay = context.dataIndex * 300 + context.datasetIndex * 100;
          }
          return delay;
        },
      },  
      
      scales: {
        lineax:{
          type: 'linear',
          position: 'right',
          beginAtZero: true,
          display: false,
          min: 0,
          max: 100,
          grid: {
            display: false,
          },
          
        },
        x:{
          ticks:{
            font: {
              family: "'Lato', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
              size: 15,
              weight: 'bold',
            },
          },
          
          stacked: true,
          grid: {
            display: false,
            borderWidth: 1,
            borderColor: '#000000',
          },
        },
        bary: {
          beginAtZero: true,
          min: 0,
          max: 1000000,
          display: false,
          grid: {
            display: false,
          },
          
        }
    },
    plugins: {
      htmlLegend: {
        // ID of the container to put the legend in
        containerID: 'legend-container-first',
      },
      legend: {
        display: false,
      },
      tooltip:{
        enabled: false,
      },
      datalabels: {
        font: {
          family: "'Lato', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          size: 15,
          weight: 'bold',
        },
      },
    },
    
  };

  var ctx = document.getElementById("graphOne").getContext("2d");


    var myLineChart = new Chart(ctx, {
      type: "bar",
      data: data,
      options: chartOptionsOne,
      plugins: [htmlLegendPlugin, ChartDataLabels],
    });
    
  </script>
    <!-- Chart One JS End -->
<!-- Chart Two JS Start -->
<script>
    const getOrCreateLegendListTwo = (chart, id) => {
      const legendContainerTwo = document.getElementById(id);
      let listContainerTwo = legendContainerTwo.querySelector('ul');

      if (!listContainerTwo) {
        listContainerTwo = document.createElement('ul');
        listContainerTwo.style.display = 'flex';
        listContainerTwo.style.flexDirection = 'column';
        listContainerTwo.style.margin = 0;
        listContainerTwo.style.padding = 0;

        legendContainerTwo.appendChild(listContainerTwo);
      }

      return listContainerTwo;
    };

    const htmlLegendPluginTwo = {
      id: 'htmlLegendTwo',
      afterUpdate(chart, args, options) {
        const ul = getOrCreateLegendListTwo(chart, options.containerID);

        // Remove old legend items
        while (ul.firstChild) {
          ul.firstChild.remove();
        }

        // Reuse the built-in legendItems generator
        const items = chart.options.plugins.legend.labels.generateLabels(chart);

        items.forEach(item => {
          const li = document.createElement('li');
        // li.style.alignItems = 'center';
          li.style.cursor = 'pointer';
          li.style.display = 'flex';
          li.style.alignItems = 'center';
          li.style.justifyContent = 'center';
          li.style.marginLeft = '0';
          li.style.background = item.fillStyle;
          li.style.borderColor = item.strokeStyle;
          li.style.color = item.fontColor;
          li.style.marginBottom = '10px';
          li.style.borderRadius = '10px';
          li.style.height = '40px';
          li.style.width = '170px';
          li.style.padding = '10px';
          
        //  li.style.padding = 0;

          li.onclick = () => {
            const {type} = chart.config;
            if (type === 'pie' || type === 'doughnut') {
              // Pie and doughnut charts only have a single dataset and visibility is per item
              chart.toggleDataVisibility(item.index);
            } else {
              chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
            }
            chart.update();
          };

          // Color box
          const boxSpan = document.createElement('span');
          boxSpan.style.background = item.fillStyle;
          boxSpan.style.borderColor = item.strokeStyle;
          boxSpan.style.borderWidth = item.lineWidth + 'px';
          boxSpan.style.display = 'inline-block';
          boxSpan.style.height = '20px';
          boxSpan.style.marginRight = '10px';
          boxSpan.style.width = '20px';

          // Text
          const textContainer = document.createElement('p');
          textContainer.style.color = item.fontColor;
          textContainer.style.margin = 0;
          textContainer.style.padding = 0;
        // textContainer.style.textDecoration = item.hidden ? 'line-through' : '';

          const text = document.createTextNode(item.text);
          textContainer.appendChild(text);

        //  li.appendChild(boxSpan);
          li.appendChild(textContainer);
          ul.appendChild(li);
        });
      }
    };

    
    var datatwo = {
      type: 'bar',
      labels: [
        "30",
        "32",
        "35",
        "36",
        "37",
        "38",
        "39",
        "40",
        "41",
        "42",
        "43",
        "52",
        "52+",
        
      ],
      datasets: [
        {
          label: 'Total amount received',
          backgroundColor: "#9F6F47",
          borderColor: "#9F6F47",
          color: "#9F6F47",
          type: 'line',
          pointRadius: 4,
          yAxisID: "amountY",
          data: [0, 200000, 1500000, 1800000, 2100000, 3100000, 3400000, 3700000, 4700000, 5000000, 5300000, 6300000],
          labels: ["", "HK$ 200,000", "HK$ 1,500,000", "HK$ 1,800,000", "HK$ 2,100,000", "HK$ 3,100,000", "HK$ 3,400,000", "HK$ 3,700,000", "HK$ 4,700,000", "HK$ 5,000,000", "HK$ 5,300,000", "HK$ 6,300,000"],
          datalabels:{
            color: '#9F6F47',
            anchor: 'end',
            align: 'top',
            offset: '5',
            opacity: function(context) {
              if(context.chart.data.datasets[context.datasetIndex].data[context.dataIndex] == 0){
                return 0;
              } 
              
            },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
          }
        },
        {
          label: "Amount paid",
          backgroundColor: "#D2DADC",
          borderColor: "#D2DADC",
          hoverBackgroundColor: "#294A4A",
          hoverBorderColor: "#294A4A",
          borderWidth: 1,
          categoryPercentage: 1.1,
          type: 'bar',
          borderRadius: 10,
          yAxisID: "allbarY",
          data: [1000000],
          labels: ["HK$ 1,000,000"],
          datalabels:{
            color: '#ffffff',
            anchor: 'end',
            align: 'start',
            offset: '10',
            opacity: function(context) {
              if(context.chart.data.datasets[context.datasetIndex].data[context.dataIndex] == 0){
                return 0;
              } 
              return context.active ? 1 : 1; 
            },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
          }
        },
        {
          label: "Amount received",
          backgroundColor: "#EBE2D9",
          borderColor: "#EBE2D9",
          hoverBackgroundColor: "#9F6F47",
          hoverBorderColor: "#9F6F47",
          borderWidth: 1,
          categoryPercentage: 1.1,
          type: 'bar',
          borderRadius: 10,
          yAxisID: "allbarY",
          data: [0,200000,1300000,300000,300000,1000000,300000,300000,1000000,300000,300000,1000000,1300000],
          labels: ["", "HK$ 200,000", "HK$ 1,300,000", "HK$ 300,000", "HK$ 300,000", "HK$ 1,000,000", "HK$ 300,000", "HK$ 300,000", "HK$ 1,000,000", "HK$ 300,000", "HK$ 300,000", "HK$ 1,000,000", "HK$ 1,300,000"],
          datalabels:{
            color: '#ffffff',
            anchor: 'start',
            align: 'top',
            offset: '5',
            opacity: function(context) {
              if(context.chart.data.datasets[context.datasetIndex].data[context.dataIndex] == 0){
                return 0;
              } 
              return context.active ? 1 : 1; 
            },
            formatter: function(value, context) {
              return context.dataset.labels[context.dataIndex]; 
            },
          }
        },

        {
          label: "X data",
          type: 'bar',
          categoryPercentage: 1.1,
          data: [1300000,1300000,1300000,1300000,1300000,1300000,1300000,1300000,1300000,1300000,1300000,1300000,1300000],
          backgroundColor: [
              'rgba(255, 255, 255, 0)',
          ],
          borderColor: [
              'rgba(255, 255, 255, 0)',
          ],
          yAxisID: "allbarY",
       //  borderRadius: 5,
         hoverBackgroundColor: 'rgba(245, 245, 245, 1)',
         hoverBorderColor: 'rgba(245, 245, 245, 1)',
         borderWidth: 1,
          datalabels:{
            display: false,
            opacity: function(context) {
              return context.active ? 0 : 0;
            }
          }
          
        }
      ]
    };
   // var delayed;
    var chartOptionsTwo = {
   //   responsive: true,
    //  maintainAspectRatio: false,
   //   aspectRatio: 2,
      onHover : function(evt) {
        var activePoint = graphTwo.getElementsAtEventForMode(evt, 'nearest', {
          intersect: true
        }, false);
        document.getElementById("data1").classList.remove("bottom-text-show");
        document.getElementById("data2").classList.remove("bottom-text-show");
        document.getElementById("data3").classList.remove("bottom-text-show");
        document.getElementById("data4").classList.remove("bottom-text-show");
        document.getElementById("data5").classList.remove("bottom-text-show");
        document.getElementById("data6").classList.remove("bottom-text-show");

        document.getElementById("graph_data_1").classList.remove("bottom-text-show");
        document.getElementById("graph_data_2").classList.remove("bottom-text-show");
        document.getElementById("graph_data_3").classList.remove("bottom-text-show");
        document.getElementById("graph_data_4").classList.remove("bottom-text-show");
        document.getElementById("graph_data_5").classList.remove("bottom-text-show");
        document.getElementById("graph_data_6").classList.remove("bottom-text-show");
        document.getElementById("graph_data_7").classList.remove("bottom-text-show");
        document.getElementById("graph_data_8").classList.remove("bottom-text-show");
        document.getElementById("graph_data_9").classList.remove("bottom-text-show");
        document.getElementById("graph_data_10").classList.remove("bottom-text-show");
        document.getElementById("graph_data_11").classList.remove("bottom-text-show");
        document.getElementById("graph_data_12").classList.remove("bottom-text-show");
        document.getElementById("graph_data_13").classList.remove("bottom-text-show");

        if (activePoint.length > 0) {
          var clickedElementindex = activePoint[0].index;
          if(clickedElementindex == 0){
            document.getElementById("graph_data_1").classList.add("bottom-text-show");
          } else if (clickedElementindex == 1){
          document.getElementById("graph_data_2").classList.add("bottom-text-show");
          } else if (clickedElementindex == 2){
          document.getElementById("graph_data_3").classList.add("bottom-text-show");
          } else if (clickedElementindex == 3){
          document.getElementById("graph_data_4").classList.add("bottom-text-show");
          } else if (clickedElementindex == 4){
          document.getElementById("graph_data_5").classList.add("bottom-text-show");
          } else if (clickedElementindex == 5){
          document.getElementById("graph_data_6").classList.add("bottom-text-show");
          } else if (clickedElementindex == 6){
          document.getElementById("graph_data_7").classList.add("bottom-text-show");
          } else if (clickedElementindex == 7){
          document.getElementById("graph_data_8").classList.add("bottom-text-show");
          } else if (clickedElementindex == 8){
          document.getElementById("graph_data_9").classList.add("bottom-text-show");
          } else if (clickedElementindex == 9){
          document.getElementById("graph_data_10").classList.add("bottom-text-show");
          } else if (clickedElementindex == 10){
          document.getElementById("graph_data_11").classList.add("bottom-text-show");
          } else if (clickedElementindex == 11){
          document.getElementById("graph_data_12").classList.add("bottom-text-show");
          } else if (clickedElementindex == 12){
          document.getElementById("graph_data_13").classList.add("bottom-text-show");
          } 
        }
      },
      animation: {
        onComplete: () => {
          delayed = true;
        },
        delay: (context) => {
          let delay = 0;
          if (context.type === 'data' && context.mode === 'default' && !delayed) {
            delay = context.dataIndex * 300 + context.datasetIndex * 100;
          }
          return delay;
        },
      },  
      scales: {
        amountY:{
        type: 'linear',
        position: 'right',
        beginAtZero: true,
        display: false,
        min: 0,
        max: 7400000,
        grid: {
          display: false,
        },
        
      },
      x:{
        // title: {
        //   display: true,
        //   text: 'Year',
        //   align: 'start',
        //   padding: {
        //     top: 20,
        //     left: -100,
        //     right: 0,
        //     bottom: 0
        //   }
        // },
        ticks:{
          font: {
            family: "'Lato', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            size: 15,
            weight: 'bold',
          },
        },
        stacked: true,
        grid: {
          display: false,
          borderWidth: 1,
          borderColor: '#000000',
        },
        
      },
      allbarY: {
        beginAtZero: true,
        min: 0,
        max: 1300000,
        display: false,
        grid: {
          display: false,
        }
      }
    },
    plugins: {
      htmlLegendTwo: {
        // ID of the container to put the legend in
        containerID: 'legend-container-Two',
      },
      legend: {
        display: false,
      },
      tooltip:{
        enabled: false,
      },
      datalabels: {
        // formatter: (value) => {
        //   //return value + '%';
        //   return 'HK$' + value;
        // },
        font: {
          family: "'Lato', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          size: 12,
          weight: "bold",
        },
      },
    },
    
  };

      var ctx = document.getElementById("canvas").getContext("2d");
      var graphTwo = new Chart(ctx, {
        type: "bar",
        data: datatwo,
        options: chartOptionsTwo,
        plugins: [htmlLegendPluginTwo, ChartDataLabels],
      });
</script>
<!-- Chart Two JS End -->

<!-- Chart Slider Init JS Start -->
<script>
  jQuery(document).ready(function(){
    jQuery('.graph-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      prevArrow: jQuery('.prev-arrow'),
      nextArrow: jQuery('.next-arrow')
    });
  });

  jQuery(document).ready(function () {


    jQuery('.graph-one-cta').click(function (e) {
        //e.preventDefault();
        jQuery('html, body').animate({
            scrollTop: jQuery("#request_free_quote").offset().top
        }, 2000);

        jQuery("#wpforms-2000-field_12 option").val("Legacy Planning").text("Legacy Planning");
        jQuery(".choices__list.choices__list--single").remove();
        jQuery(".choices__inner").append('<div class="choices__list choices__list--single"><div class="choices__item choices__item--selectable" data-item="" data-id="9" data-value="Legacy planning" data-custom-properties="null" aria-selected="true" data-deletable="">Legacy planning<button type="button" class="choices__button" aria-label="Remove item: Legacy planning" data-button="">Remove item</button></div></div>');
        });

        jQuery('.graph-two-cta').click(function (e) {
        //e.preventDefault();
        jQuery('html, body').animate({
            scrollTop: jQuery("#request_free_quote").offset().top
        }, 2000);

        jQuery("#wpforms-2000-field_12 option").val("Critical illness protection").text("Critical illness protection");
        jQuery(".choices__list.choices__list--single").remove();
        jQuery(".choices__inner").append('<div class="choices__list choices__list--single"><div class="choices__item choices__item--selectable" data-item="" data-id="3" data-value="Critical illness protection" data-custom-properties="null" aria-selected="true" data-deletable="">Critical illness protection<button type="button" class="choices__button" aria-label="Remove item: Critical illness protection" data-button="">Remove item</button></div></div>');
        });
});
</script>    
<!-- Chart Slider Init JS End -->
